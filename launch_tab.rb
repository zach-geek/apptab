#!/usr/bin/env ruby
require 'logger'
require 'json'

# l = Logger.new("/tmp/apptab.log")
# l.info("ok")


b = STDIN.read(4).unpack("i")[0]
# l.info("Size: #{b}")
s = STDIN.read(b)
# l.info("Read: #{s}")
url = JSON.parse(s)["url"]

Process.detach(spawn("google-chrome", "--app=#{url}"))
