var currentId = 0

function createPopup(url) {
  chrome.app.window.create("popup.html",
    {
       id: "framelessWinID" + currentId++,
       innerBounds: {
         width: 800,
         height:600,
         left: 600,
         minWidth: 220,
         minHeight: 220
      }
    },
    function (win) {
      win.contentWindow.apptab = url;
      win.title = win.contentWindow.document.title
    }
  );
}

chrome.runtime.onMessageExternal.addListener(
  function(request, sender, sendResponse) {
    if (request.apptab)
    {
      console.log("Creating window for: " + request.apptab)
      createPopup(request.apptab);
      sendResponse({farewell: "goodbye"});
    }
  });
