# App tab

This is a little Chrome extension that adds a button to launch the current tab in it's own App window (i.e., without an address bar or tab bar).
